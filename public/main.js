document.querySelector('#register').addEventListener('click', () => {
    let isValid = true;

    const data = {
        lastName: document.querySelector('#lastName').value,
        firstName: document.querySelector('#firstName').value,
        age: document.querySelector('#age').value,
        specialization: document.querySelector('#specialization').value
    }
    // console.log(data);
    if (!data.lastName) {
        toastr.error("Nu ati introdus numele");
        isValid = false;
    }
    if (!data.firstName) {
        toastr.error("Nu ati introdus prenumele");
        isValid = false;
    }
    if (!data.age) {
        toastr.error('Nu ati introdus varsta');
        isValid = false;
    } else if (!data.age.match(/^[0-9]*$/)) {
        toastr.error('Varsta trebuie sa contina doar cifre');
        isValid = false;
    }

    if (data.specialization === 'Specializare') {
        toastr.error('Trebuie sa alegeti o specializare');
        isValid = false;
    }
    if (isValid === true) {
        // console.log('Sunt corecte');
        let contestant = {
            firstName: data.firstName,
            lastName: data.lastName,
            age: data.age,
            specialization: data.specialization
        }
        axios.post('/api/participants', data)
            .then((response) => {
                toastr.success(response.data.message);
                document.querySelector('#lastName').value = '';
                document.querySelector('#firstName').value = '';
                document.querySelector('#age').value = '';
                document.querySelector('#specialization').value = 'Specializare';
            })
            .catch(error => {
                if (error.response.status === 500) {
                    toastr.error(error.response.data.message)
                } else {
                    error.response.data.errors.forEach(element => {
                        toastr.error(element);
                    });
                }

            })
    }


})